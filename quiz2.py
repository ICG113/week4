def blueChange(picture):
  for x in range (0,400):
    for y in range (0,400):
      pixel = getPixelAt(picture,x,y)
      #Question 1
      #blu = getBlue(pixel)
      
      #if blu < 150:
      #  setColor(pixel,white)
      
      #Question 2
      #blu = getBlue(pixel) * 2
      #rd = getRed(pixel) / 2
      #grn = getGreen(pixel) / 2
      #newColor = makeColor(rd,grn,blu)
      #setColor(pixel,newColor)
      
      #Question 3
      #blu = getBlue(pixel) / 2
      #rd = getRed(pixel) * 2
      #grn = getGreen(pixel) / 2
      #newColor = makeColor(rd,grn,blu)
      #setColor(pixel,newColor)
      
#Question 4
#def greyScale(picture):
#  for pixel in getPixels(picture):
#     intensity = (getRed(pixel)+getGreen(pixel)+getBlue(pixel))/3
#     setColor(pixel,makeColor(intensity,intensity,intensity))
#          
#     rd = 255 - getRed(pixel)
#     grn = 255 - getGreen(pixel)
#     blu = 255 - getBlue(pixel)
     
#Question 5
#def greyScale(picture):
#  for pixel in getPixels(picture):
#     intensity = (getRed(pixel)+getGreen(pixel)+getBlue(pixel))/3
#     setColor(pixel,makeColor(intensity,intensity,intensity))
#     
#     rd = getRed(pixel) + 75
#     grn = getGreen(pixel) + 75
#     blu = getBlue(pixel) + 75
#     
#     rd = 255 - getRed(pixel)
#     grn = 255 - getGreen(pixel)
#     blu = 255 - getBlue(pixel)

#Question 6
#def topHalfBlk(picture):
#  width = getWidth(picture)
#  height = getHeight(picture)/2
#  for y in range(0, height):
#    for x in range(0,width):
#      pixel = getPixelAt(picture,x,y)
#      setColor(pixel,black)
         
#Question 7
def mirrorHalf(picture):
  width = getWidth(picture)
  height = getHeight(picture)
  for y in range(0, height/2):
    for x in range(0,width):
      pixel = getPixelAt(picture,x,y)
      color = getColor(pixel)
      newY = (height - 1) - y
      targetPixel = getPixelAt(picture,x,newY)
      setColor(targetPixel,color)
    
file = pickAFile()
picture = makePicture(file)
mirrorHalf(picture)
show(picture) 