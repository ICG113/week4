#setMediaPath("Users/icutter2016/Documents/week4/database/")
#input1 = raw_input("Input: ")
#input2 = raw_input("Input: ")
#file = open("Users/icutter2016/Documents/week4/database/data.txt", "w")
#file.write("InputOne: %s \n InputTwo: %s" % input1, input2)



print "      ____        _____     _____ "
print "     |   __ \    / ____|   /  ___|"
print "     |  |__) |  |  |  __     |  (___  "
print "     |   _  /   |  | |_  |    \___ \ "
print "     |  | \ \   |  |__|  |   ____)  |"
print "     |__|  \_\  \____|   |_____/ "
print "      RETRO GAME STATION"
print "          Game Database"
print "Type 'help' for a list of commands."
print ""
print ""
print " COPYRIGHT CUTTERCORP 1998"

helpInput = "help"
helpInput = raw_input("> ")

if (helpInput == "help"):
  print "To add a game to the database, type 'add,' then specify its console, name, company, developer, publisher, year of release, and condition."
  print "To remove a game from the database, type 'delete,' then type the name of the game you wish to remove."
  print "If you wish to search for a game by its name, type 'search,' then the name of the game you're looking for."
  print "For more specific searches, type 'searchCons [console],' 'searchComp [company],' 'searchDev [developer],' 'searchPub [publisher],' 'searchYear [year],' or 'searchCond [condition].'"
  print 
  


